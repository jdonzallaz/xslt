package com.jdonzallaz.xslt;

import net.sf.saxon.s9api.*;
import org.apache.commons.io.FilenameUtils;

import javax.xml.transform.stream.StreamSource;
import java.io.*;

import static java.lang.System.exit;

/**
 * XSLT is an utils to transform xml files with xslt.
 * Provide an xml file, a xslt file and an optional output file.
 * Default output file is xml-filename-without-extension.html.
 *
 * @author Jonathan Donzallaz
 * @version 1.1.0
 * @since 30.05.2018
 */
class XSLT {
    public static void main(String argv[]) {
        if (argv.length < 2) {
            System.out.println("Transform xml file using:\n" +
                    "xslt <xml-file.xml> <xslt-file.xsl> [output-file]");
            exit(1);
        }

        String xmlFilename = argv[0];
        String xsltFilename = argv[1];
        String outputFilename = argv.length > 2 ? argv[2] : FilenameUtils.removeExtension(xmlFilename) + ".html";

        File xmlFile = new File(xmlFilename);
        File stylesheet = new File(xsltFilename);
        File outputFile = new File(outputFilename);

        // Init the processor
        Processor processor = new Processor(false);
        XsltCompiler compiler = processor.newXsltCompiler();

        try {
            // Set xsl source
            XsltExecutable xsl = compiler.compile(new StreamSource(stylesheet));
            XsltTransformer trans = xsl.load();

            // Set xml source
            trans.setSource(new StreamSource(xmlFile));

            // Output parameters
            Serializer serializer = processor.newSerializer();
            serializer.setOutputProperty(Serializer.Property.METHOD, "html");
            serializer.setOutputProperty(Serializer.Property.INDENT, "yes");
            serializer.setOutputProperty(Serializer.Property.VERSION, "5");
            serializer.setOutputFile(outputFile);
            trans.setDestination(serializer);

            // Launch transformation
            trans.transform();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            exit(1);
        }
    }
}